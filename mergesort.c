#include <stdio.h>
#include <stdlib.h>
#include <time.h> //needed for benchmarking

void print_list(float *list, int len)
{
    // print any list of longs in format [ 0 10 100 ]
    int i;
    printf("[ ");
    for (i = 0; i < len; i++) { printf("%f ", list[i]); }
    printf("]\n");
}

//used to sort arrays of size 4
void bubblesort(float arr[], int num) {
   if(num <= 1){ return; }
   int i, j;
   float temp;
 
   for (i = 1; i < num; i++) {
      for (j = 0; j < num - 1; j++) {
         if (arr[j] > arr[j + 1]) {
            temp = arr[j];
            arr[j] = arr[j + 1];
            arr[j + 1] = temp;
         }
      }
   }
 
}


// merge subroutine
void merge(float *list, int l_start, int l_end, int r_start, int r_end){

    // temporary array sizes
    int l_len = l_end - l_start;
    int r_len = r_end - r_start;
 
    // temporary lists for comparison
    float l_half[l_len];
    float r_half[r_len];
 
    int i = l_start;
    int l = 0;
    int r = 0;
 
    // copy values into temporary lists
    for (i=l_start; i < l_end; i++, l++) { l_half[l] = list[i]; }
    for (i=r_start; i < r_end; i++, r++) { r_half[r] = list[i]; }
 
    // merge the values back into positions in main list 
    for (i=l_start, r=0, l=0; l < l_len && r < r_len; i++)
    {
        // if left value < r value, move left value
        if (l_half[l] < r_half[r]) { list[i] = l_half[l]; l++; }
        // else move right value
        else { list[i] = r_half[r]; r++; }
    }
 
    // handle leftover values
    for ( ; l < l_len; i++, l++) { list[i] = l_half[l]; }
    for ( ; r < r_len; i++, r++) { list[i] = r_half[r]; }
}

 
// recursive mergesort
void mergesort(int left, int right, float *list)
{
    if (right - left <= 1){ return; }

    // base case
    if (right - left <= 4) { bubblesort(&(list[left]), right - left); }
    
    // get slice indices
    int l_start = left;
    int l_end = (left+right)/2;
    int r_start = l_end;
    int r_end = right;
 
    // recursive call on left half
    mergesort(l_start, l_end, list);
    // recursive call on right half
    mergesort(r_start, r_end, list);
    // merge sorted right and left halves back together
    merge(list, l_start, l_end, r_start, r_end);
}
 
void main()
{
    //populate list
    const int N = 700000;
    float list[N];
    for(int i = 0; i < N; i++){
        float r = (float)rand()/(float)RAND_MAX;
        list[i] = r;
    }
    
    //Benchmark code:
    float startTime = (float)clock()/CLOCKS_PER_SEC;
    mergesort(0, N, list);
    float endTime = (float)clock()/CLOCKS_PER_SEC;
    printf("Time elapsed: %f", endTime - startTime);

    /*
    // predefined test case
    int test_length = 15;
    float list[15] = { 9.3, 1.2, 9.3, 1.7, -6.4, 8.0, 2.0, 5.0, 1.4, 4.13, 11.00001, 7.6, 10.0, 8.4, 0.0 };
    printf("Test - Before sort: ");
    print_list(list, test_length);
    mergesort(0, test_length, list);
    printf("Test - After sort: ");
    print_list(list, test_length); */
}
