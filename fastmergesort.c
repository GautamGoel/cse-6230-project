#include <stdio.h>
#include <stdlib.h>
#include <immintrin.h> //SSE Intrinsics
#include <time.h> //needed for benchmarking

void print_SIMD(__m128 v){
        float out[4];
        _mm_store_ps(out, v);
        for(int i = 0; i < 4; i++){
                printf("%f ", out[3-i]);
        }
        printf("\n");
}

void four_way_bitonic_merge(float *in1, float *in2, float *out1, float *out2){ 

        __m128 A = _mm_loadu_ps(in1);
        __m128 B = _mm_loadu_ps(in2);

        B = _mm_shuffle_ps(B, B, _MM_SHUFFLE(0, 1, 2, 3)); //reverse B

        /*
        printf("A is: ");
        print_SIMD(A);
        printf("B is: ");
        print_SIMD(B);
        printf("\n");
        */

        //Phase 1
        __m128 L1 = _mm_min_ps(A, B);
        __m128 H1 = _mm_max_ps(A, B);

        /*
        printf("L1 is: ");
        print_SIMD(L1);
        printf("H1 is: ");
        print_SIMD(H1);
        printf("\n");
        */

        __m128 L1p = _mm_shuffle_ps(H1, L1, _MM_SHUFFLE(3, 2, 3, 2)); 
        __m128 H1p = _mm_shuffle_ps(H1, L1, _MM_SHUFFLE(1, 0, 1, 0));

        /*
        printf("L1p is: ");
        print_SIMD(L1p);
        printf("H1p is: ");
        print_SIMD(H1p);
        printf("\n");
        */

        //Phase 2
        __m128 L2 = _mm_min_ps(L1p, H1p);
        __m128 H2 = _mm_max_ps(L1p, H1p);

        /*
        printf("L2 is: ");
        print_SIMD(L2);
        printf("H2 is: ");
        print_SIMD(H2);
        printf("\n");
        */

        __m128 I1 = _mm_shuffle_ps(H2, L2, _MM_SHUFFLE(3, 1, 3, 1));
        __m128 I2 = _mm_shuffle_ps(H2, L2, _MM_SHUFFLE(2, 0, 2, 0));
        
        /*
        printf("I1 is: ");
        print_SIMD(I1);
        printf("I2 is: ");
        print_SIMD(I2);
        printf("\n");
        */
        
        __m128 L2p = _mm_shuffle_ps(I1, I1, _MM_SHUFFLE(3, 1, 2, 0)); 
        __m128 H2p = _mm_shuffle_ps(I2, I2, _MM_SHUFFLE(3, 1, 2, 0));

        /*
        printf("L2p is: ");
        print_SIMD(L2p);
        printf("H2p is: ");
        print_SIMD(H2p);
        printf("\n");
        */

        //Phase 3
        __m128 L3 = _mm_min_ps(L2p, H2p);
        __m128 H3 = _mm_max_ps(L2p, H2p);

        /*
        printf("L3 is: ");
        print_SIMD(L3);
        printf("H3 is: ");
        print_SIMD(H3);
        printf("\n");
        */

        //Need to interleave L3 and H3. First, we collect the right items into O1 and O2;
        //then we put them in the right order

        __m128 O1;
        __m128 O2;

        O1 = _mm_shuffle_ps(H3, L3, _MM_SHUFFLE(3, 2, 2, 3)); 
        O2 = _mm_shuffle_ps(L3, H3, _MM_SHUFFLE(0, 1, 1, 0));

        O1 = _mm_shuffle_ps(O1, O1, _MM_SHUFFLE(1, 2, 0, 3)); 
        O2 = _mm_shuffle_ps(O2, O2, _MM_SHUFFLE(3, 0, 2, 1));

        /*        
        printf("O1 is: ");
        print_SIMD(O1);
        printf("O2 is: ");
        print_SIMD(O2);
        printf("\n");
        */

        _mm_store_ps(out1, O1);
        _mm_store_ps(out2, O2); 

}

void print_list(float *list, int len)
{

    int i;
    printf("[ ");
    for (i = 0; i < len; i++) { printf("%f ", list[i]); }
    printf("]\n");
}

//used to sort arrays of size 4
void bubblesort(float arr[], int num) {
   if(num <= 1){ return; }
   int i, j;
   float temp;
 
   for (i = 1; i < num; i++) {
      for (j = 0; j < num - 1; j++) {
         if (arr[j] > arr[j + 1]) {
            temp = arr[j];
            arr[j] = arr[j + 1];
            arr[j + 1] = temp;
         }
      }
   }
 
}

void print_arr(float arr[4]){
    for(int i = 0; i < 4; i++){
        printf("%f ", arr[i]);
    }
    printf("\n");
}

// merge subroutine, assuming l_len, r_len % 4 == 0
void merge2(float *list, int l_start, int l_end, int r_start, int r_end){

    // temporary array sizes
    int l_len = l_end - l_start;
    int r_len = r_end - r_start;
 
    // temporary lists for comparison
    float l_half[l_len] __attribute__((aligned(16)));
    float r_half[r_len] __attribute__((aligned(16)));
 
    int i = l_start;
    int l = 0;
    int r = 0;
 
    // copy values into temporary lists
    for (i=l_start; i < l_end; i++, l++) { l_half[l] = list[i]; }
    for (i=r_start; i < r_end; i++, r++) { r_half[r] = list[i]; }

    float out1[4], out2[4] __attribute__((aligned(16)));

    four_way_bitonic_merge(l_half, r_half, out1, out2);

    //put in first four values
    for(i = 0; i < 4; i++){
        list[l_start + i] = out1[i];
    }
    

    // merge the other values back into positions in main list 
    for (i=l_start + 4, r=4, l=4; l < l_len && r < r_len;){
 
        // if left value < r value, move left value
        if (l_half[l] < r_half[r]){ 
            four_way_bitonic_merge(&(l_half[l]), out2, out1, out2);
            for(int j = 0; j < 4; j++){
                list[i + j] = out1[j];
            }
            l+=4;
            i+=4;
           // print_list(list, r_end - l_start); 
        }
        // else move right value
        else { 
            four_way_bitonic_merge(&(r_half[r]), out2, out1, out2);
             for(int j = 0; j < 4; j++){
                list[i + j] = out1[j];
            }
            r+=4;
            i+=4;
            //print_list(list, r_end - l_start);
        }
    }
        

       //Fill in leftovers
        while (l < l_len){ 
            four_way_bitonic_merge(&(l_half[l]), out2, out1, out2);
            for(int j = 0; j < 4; j++){
                list[i + j] = out1[j];
            }
            l+=4;
            i+=4;
            //print_list(list, r_end - l_start); 
        }
        while(r < r_len) { 
            four_way_bitonic_merge(&(r_half[r]), out2, out1, out2);
             for(int j = 0; j < 4; j++){
                list[i + j] = out1[j];
            }
            r+=4;
            i+=4;
            //print_list(list, r_end - l_start);
        }
    

    //last but not least!
    for(int j = 0; j < 4; j++){
                list[i + j] = out2[j];
    }  

}


// merge subroutine
void merge(float *list, int l_start, int l_end, int r_start, int r_end){

    // temporary array sizes
    int l_len = l_end - l_start;
    int r_len = r_end - r_start;
 
    // temporary lists for comparison
    float l_half[l_len];
    float r_half[r_len];
 
    int i = l_start;
    int l = 0;
    int r = 0;
 
    // copy values into temporary lists
    for (i=l_start; i < l_end; i++, l++) { l_half[l] = list[i]; }
    for (i=r_start; i < r_end; i++, r++) { r_half[r] = list[i]; }
 
    // merge the values back into positions in main list 
    for (i=l_start, r=0, l=0; l < l_len && r < r_len; i++)
    {
        // if left value < r value, move left value
        if (l_half[l] < r_half[r]) { list[i] = l_half[l]; l++; }
        // else move right value
        else { list[i] = r_half[r]; r++; }
    }
 
    // handle leftover values
    for ( ; l < l_len; i++, l++) { list[i] = l_half[l]; }
    for ( ; r < r_len; i++, r++) { list[i] = r_half[r]; } 
}

 
// recursive mergesort
void mergesort(int left, int right, float *list){
    if (right - left <= 1){ return; }

    // base case
    if (right - left <= 4) { bubblesort(&(list[left]), right - left); }
 
    // get slice indices
    int l_start = left;
    int l_end = (left+right)/2;
    int r_start = l_end;
    int r_end = right;
    
    //printf("%d %d %d %d\n", l_start, l_end, r_start, r_end);
 
    // recursive call on left half
    mergesort(l_start, l_end, list);

    // recursive call on right half
    mergesort(r_start, r_end, list);


    // merge sorted right and left halves back together
    if((r_end - r_start) % 4 == 0 && (l_end-l_start) % 4 == 0){
    merge2(list, l_start, l_end, r_start, r_end);
    }
    else{
        merge(list, l_start, l_end, r_start, r_end);
    }
}
 
void main()
{
    //populate list
    const int N = 700000;
    float list[N];
    for(int i = 0; i < N; i++){
        float r = (float)rand()/(float)RAND_MAX;
        list[i] = r;
    }
    
    //Benchmark code:
    float startTime = (float)clock()/CLOCKS_PER_SEC;
    mergesort(0, N, list);
    float endTime = (float)clock()/CLOCKS_PER_SEC;
    printf("Time elapsed: %f", endTime - startTime);
    
    /*
    // predefined test case
    int test_length = 15;
    float list[15] = { 9.3, 1.2, 9.3, 1.7, -6.4, 8.0, 2.0, 5.0, 1.4, 4.13, 11.00001, 7.6, 10.0, 8.4, 0.0 };
    printf("Test - Before sort: ");
    print_list(list, test_length);
    mergesort(0, test_length, list);
    printf("Test - After sort: ");
    print_list(list, test_length); */
    
}

